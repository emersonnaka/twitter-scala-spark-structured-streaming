# Twitter Scala Spark Structured Streaming

Process Twitter messages using Spark Structured Streaming integrated Kafka

## Goals

* Study data streaming process with Kafka
* Build data pipeline
* Consume [Twitter Streaming Kafka](https://gitlab.com/emersonnaka/twitter-streaming-kafka) application

## Requirements

* Scala 2
* Maven

## Execution

* Main class: `src/main/scala/com/nakashima/application/TwitterScalaSparkStructuredStreaming.scala`
* To create a step to transform data, it's necessary create an implemented class Step and insert it into `buildStepList` method at `src/main/scala/com/nakashima/application/config/ProcessPipeline.scala`
