ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "twitter-scala-spark-structured-streaming",
    libraryDependencies += "com.typesafe" % "config" % "1.2.1",
    // https://mvnrepository.com/artifact/org.apache.spark/spark-sql
    libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.2.1",
    // https://mvnrepository.com/artifact/org.apache.spark/spark-sql-kafka-0-10
    libraryDependencies += "org.apache.spark" %% "spark-sql-kafka-0-10" % "3.2.1",
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.12",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % "test"

)

