package com.nakashima.application.helper

import com.nakashima.application.config.Context
import org.apache.spark.sql.Encoders

object ContextUtil extends SparkTest {

  val Path = "src/test/resources/tweet.json"

  def build(): Context = {
    val context = new Context(spark)
    val ds = spark.read.text(Path)
    context.twitterTable.ds_(ds)
    context
  }

  def build(json: String): Context = {
    val context = new Context(spark)
    val jsonList = List(json)
    val createDs = spark.createDataset(jsonList)(Encoders.STRING)
    val ds = spark.read.json(createDs)

    context.twitterTable.ds_(ds)
    context
  }

}
