package com.nakashima.application.helper

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

trait SparkTest {

  val conf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("Unit Test Data Frame")
    .set("spark.driver.host", "localhost")

  val spark = SparkSession.builder()
    .config(conf)
    .config("spark.sql.sources.partitionOverwriteMode", "dynamic")
    .config("spark.driver.allowMultipleContexts", "true")
    .getOrCreate()

}
