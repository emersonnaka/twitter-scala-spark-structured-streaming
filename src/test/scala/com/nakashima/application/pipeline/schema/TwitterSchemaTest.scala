package com.nakashima.application.pipeline.schema

import com.nakashima.application.enumeration.TwitterMessageEnum.{HashtagEntities, Id, UserMentionEntities}
import com.nakashima.application.config.Context
import com.nakashima.application.helper.ContextUtil
import org.apache.spark.sql.functions.{col, explode, size}
import org.scalatest.funsuite.AnyFunSuite

class TwitterSchemaTest extends AnyFunSuite {

  val context: Context = ContextUtil.build()
  new TwitterSchema().process(context)

  test("Check any column values after apply schema") {
    val ds = context.twitterTable.ds
    assert(ds.select(Id.schemaField).first.getLong(0) == 1520077564282388480L)
    assert(ds.select("user.id").first().getLong(0) == 4880544759L)
  }

  test("Check array column") {
    var ds = context.twitterTable.ds
    assert(ds.select(size(col(HashtagEntities.schemaField))).first().getInt(0) == 2)
    assert(ds.select(size(col(UserMentionEntities.schemaField))).first().getInt(0) == 1)

    ds = ds.select(explode(ds.col(UserMentionEntities.schemaField))).select("col.*")
    assert(ds.select(Id.schemaField).first().getLong(0) == 1141663136786743297L)
  }

}
