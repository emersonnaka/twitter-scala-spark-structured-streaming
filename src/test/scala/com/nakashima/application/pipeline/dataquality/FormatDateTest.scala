package com.nakashima.application.pipeline.dataquality

import com.nakashima.application.config.Context
import com.nakashima.application.enumeration.TwitterMessageEnum.CreatedAt
import com.nakashima.application.helper.ContextUtil
import org.scalatest.funsuite.AnyFunSuite

import java.sql.Timestamp

class FormatDateTest extends AnyFunSuite {

  val jsonStr = """{ "createdAt": "Apr 29, 2022 1:28:27 PM", "user": {"createdAt": "Feb 6, 2016 5:52:34 AM"}}"""
  val context: Context = ContextUtil.build(jsonStr)
  new FormatDate().process(context)

  test("check format date") {
    val ds = context.twitterTable.ds

    var ts = Timestamp.valueOf("2022-04-29 13:28:27")
    assert(ds.select(CreatedAt.schemaField).first().getTimestamp(0) == ts)

    ts = Timestamp.valueOf("2016-02-06 05:52:34")
    assert(ds.select("user.createdAt").first().getTimestamp(0) == ts)
  }

}
