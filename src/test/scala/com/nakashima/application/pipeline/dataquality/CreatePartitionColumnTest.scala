package com.nakashima.application.pipeline.dataquality

import com.nakashima.application.config.Context
import com.nakashima.application.enumeration.ConstEnum.{Day, Month, Year}
import com.nakashima.application.helper.ContextUtil
import org.scalatest.funsuite.AnyFunSuite

class CreatePartitionColumnTest extends AnyFunSuite {

  val jsonStr = """{"createdAt": "2022-04-29 13:28:27"}"""
  val context: Context = ContextUtil.build(jsonStr)
  new CreatePartitionColumn().process(context)

  test("Check partition column was created") {
    val ds = context.twitterTable.ds

    assert(ds.select(Year.value).first().getInt(0) == 2022)
    assert(ds.select(Month.value).first().getInt(0) == 4)
    assert(ds.select(Day.value).first().getInt(0) == 29)
  }

}
