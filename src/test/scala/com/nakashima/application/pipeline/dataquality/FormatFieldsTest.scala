package com.nakashima.application.pipeline.dataquality

import com.nakashima.application.config.Context
import com.nakashima.application.enumeration.TwitterMessageEnum.{CreatedAt, Id, UserMentionEntities}
import com.nakashima.application.helper.ContextUtil
import com.nakashima.application.pipeline.schema.TwitterSchema
import org.scalatest.funsuite.AnyFunSuite

class FormatFieldsTest extends AnyFunSuite {

  val context: Context = ContextUtil.build()
  new TwitterSchema().process(context)
  new CreatePartitionColumn().process(context)
  new FormatFields().process(context)

  test("Camel case to snake case") {
    val ds = context.twitterTable.ds

    assert(!ds.columns.contains(CreatedAt.schemaField))
    assert(ds.columns.contains(CreatedAt.snakeCaseField))
    assert(ds.columns.contains(Id.schemaField))
    assert(ds.columns.contains(UserMentionEntities.snakeCaseField))

    for (column <- ds.columns) {
      assert(!column.matches("[A-Z]"))
    }

    for (column <- ds.select("user.*").columns) {
      assert(!column.matches("[A-Z]"))
    }
  }

}
