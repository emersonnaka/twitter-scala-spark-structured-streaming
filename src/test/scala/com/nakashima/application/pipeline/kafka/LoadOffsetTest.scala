package com.nakashima.application.pipeline.kafka

import com.nakashima.application.enumeration.ConstEnum.{Backup, Storage}
import com.nakashima.application.enumeration.PropertiesEnum.SparkCheckpointLocation
import com.nakashima.common.PropertiesReader.getString
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.scalatest.funsuite.AnyFunSuite

class LoadOffsetTest extends AnyFunSuite {

  val loadOffset = new LoadOffset()
  val fs = FileSystem.get(new Configuration())

  test("Both offset is zero") {
    assert(loadOffset.partitionOffset == """{"twitter":{"0":0,"1":0}}""")
  }

  test("back path created") {
    val backupPath = getString(SparkCheckpointLocation.value) + Backup.value
    val path = new Path(backupPath)
    assert(fs.exists(path))
  }

  test("Offsets path has moved") {
    val offsetPath = getString(SparkCheckpointLocation.value) + Storage.value + "/offsets/"
    val path = new Path(offsetPath)
    assert(!fs.exists(path))
  }

}
