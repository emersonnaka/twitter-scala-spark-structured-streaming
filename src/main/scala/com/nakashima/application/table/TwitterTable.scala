package com.nakashima.application.table

import com.nakashima.application.enumeration.TwitterMessageEnum._
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.sql.types.DataTypes.createArrayType
import org.apache.spark.sql.types.{BooleanType, IntegerType, LongType, StringType, StructType}

class TwitterTable {

  val _schema: StructType = createSchema()

  private var _ds: Dataset[Row] = _

  def createSchema(): StructType = {
    new StructType()
      .add(CreatedAt.schemaField, StringType, false)
      .add(Id.schemaField, LongType, false)
      .add(Text.schemaField, StringType, false)
      .add(Source.schemaField, StringType, false)
      .add(IsTruncated.schemaField, BooleanType, false)
      .add(InReplyToStatusId.schemaField, LongType, false)
      .add(InReplyToUserId.schemaField, LongType, false)
      .add(IsFavorited.schemaField, BooleanType, false)
      .add(IsRetweeted.schemaField, BooleanType, false)
      .add(FavoriteCount.schemaField, IntegerType, false)
      .add(InReplayToScreenName.schemaField, StringType, false)
      .add(RetweetCount.schemaField, IntegerType, false)
      .add(IsPossiblySensitive.schemaField, BooleanType, false)
      .add(Lang.schemaField, StringType, false)
      .add(UserMentionEntities.schemaField, createArrayType(userMentionSchema()), true)
      .add(UrlEntities.schemaField, createArrayType(urlSchema()), true)
      .add(HashtagEntities.schemaField, createArrayType(hashtagSchema()), true)
      .add(MediaEntities.schemaField, createArrayType(mediaSchema()), true)
      .add(SymbolEntities.schemaField, createArrayType(symbolSchema()), true)
      .add(CurrentUserRetweetId.schemaField, IntegerType, true)
      .add(User.schemaField, userSchema(), true)
      .add(QuotedStatusId.schemaField, IntegerType, true)
  }

  private def userMentionSchema(): StructType = {
    new StructType()
      .add(Name.schemaField, StringType, true)
      .add(ScreenName.schemaField, StringType, true)
      .add(Id.schemaField, LongType, true)
      .add(Start.schemaField, IntegerType, true)
      .add(End.schemaField, IntegerType, true)
  }

  private def urlSchema(): StructType = {
    new StructType()
      .add(Url.schemaField, StringType, true)
      .add(ExpandedUrl.schemaField, StringType, true)
      .add(DisplayUrl.schemaField, StringType, true)
      .add(Start.schemaField, IntegerType, true)
      .add(End.schemaField, IntegerType, true);
  }

  private def hashtagSchema(): StructType = {
    new StructType()
      .add(Text.schemaField, StringType, true)
      .add(Start.schemaField, IntegerType, true)
      .add(End.schemaField, IntegerType, true)
  }

  private def mediaSchema(): StructType = {
    new StructType()
      .add(Id.schemaField, LongType, true)
      .add(Url.schemaField, StringType, true)
      .add(MediaUrl.schemaField, StringType, true)
      .add(MediaUrlHttps.schemaField, StringType, true)
      .add(ExpandedUrl.schemaField, StringType, true)
      .add(DisplayUrl.schemaField, StringType, true)
      .add(Sizes.schemaField, sizesSchema(), true)
      .add(Type.schemaField, StringType, true)
      .add(VideoAspectRatioWidth.schemaField, IntegerType, true)
      .add(VideoAspectRatioHeight.schemaField, IntegerType, true)
      .add(VideoDurationMillis.schemaField, IntegerType, true)
      .add(VideoVariants.schemaField, createArrayType(videoVariantsSchema()), true)
      .add(Start.schemaField, IntegerType, true)
      .add(End.schemaField, IntegerType, true)
  }

  private def sizesSchema(): StructType = {
    new StructType()
      .add(Zero.schemaField, sizeSchema(), true)
      .add(One.schemaField, sizeSchema(), true)
      .add(Two.schemaField, sizeSchema(), true)
      .add(Three.schemaField, sizeSchema(), true)
  }

  private def sizeSchema(): StructType = {
    new StructType()
      .add(Width.schemaField, IntegerType, true)
      .add(Height.schemaField, IntegerType, true)
      .add(Resize.schemaField, IntegerType, true)
  }

  private def videoVariantsSchema(): StructType = {
    new StructType()
      .add(Bitrate.schemaField, IntegerType, true)
      .add(ContentType.schemaField, StringType, true)
      .add(Url.schemaField, StringType, true)
  }

  private def symbolSchema(): StructType = {
    new StructType()
      .add(Indices.schemaField, createArrayType(IntegerType), true)
      .add(Text.schemaField, StringType, true)
  }

  private def userSchema(): StructType = {
    new StructType()
      .add(Id.schemaField, LongType, true)
      .add(Name.schemaField, StringType, true)
      .add(ScreenName.schemaField, StringType, true)
      .add(Description.schemaField, StringType, true)
      .add(DescriptionUrlEntities.schemaField, createArrayType(descriptionUrlSchema()), true)
      .add(IsContributorsEnabled.schemaField, BooleanType, true)
      .add(ProfileImageUrl.schemaField, StringType, true)
      .add(ProfileImageUrlHttps.schemaField, StringType, true)
      .add(IsDefaultProfileImage.schemaField, BooleanType, true)
      .add(IsProtected.schemaField, BooleanType, true)
      .add(FollowersCount.schemaField, IntegerType, true)
      .add(ProfileBackgroundColor.schemaField, StringType, true)
      .add(ProfileTextColor.schemaField, StringType, true)
      .add(ProfileLinkColor.schemaField, StringType, true)
      .add(ProfileSidebarFillColor.schemaField, StringType, true)
      .add(ProfileSidebarBorderColor.schemaField, StringType, true)
      .add(ProfileUseBackgroundImage.schemaField, BooleanType, true)
      .add(IsDefaultProfile.schemaField, BooleanType, true)
      .add(ShowAllInlineMedia.schemaField, BooleanType, true)
      .add(FriendsCount.schemaField, IntegerType, true)
      .add(CreatedAt.schemaField, StringType, true)
      .add(FavouritesCount.schemaField, IntegerType, true)
      .add(UtcOffset.schemaField, IntegerType, true)
      .add(ProfileBackgroundImageUrl.schemaField, StringType, true)
      .add(ProfileBackgroundImageUrlHttps.schemaField, StringType, true)
      .add(ProfileBannerImageUrl.schemaField, StringType, true)
      .add(ProfileBackgroundTiled.schemaField, BooleanType, true)
      .add(StatusesCount.schemaField, IntegerType, true)
      .add(IsGeoEnabled.schemaField, BooleanType, true)
      .add(IsVerified.schemaField, BooleanType, true)
      .add(Translator.schemaField, BooleanType, true)
      .add(ListedCount.schemaField, IntegerType, true)
      .add(IsFollowRequestSent.schemaField, BooleanType, true)
  }

  private def descriptionUrlSchema(): StructType = {
    new StructType()
      .add(Url.schemaField, StringType, true)
      .add(ExpandedUrl.schemaField, StringType, true)
      .add(DisplayUrl.schemaField, StringType, true)
  }

  def schema: StructType = {
    _schema
  }

  def ds: Dataset[Row] = {
    _ds
  }

  def ds_(ds: Dataset[Row]): Unit = {
    _ds = ds
  }

}
