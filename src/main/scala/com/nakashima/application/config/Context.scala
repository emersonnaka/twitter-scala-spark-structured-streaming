package com.nakashima.application.config

import com.nakashima.application.pipeline.kafka.LoadOffset
import com.nakashima.application.table.TwitterTable
import org.apache.spark.sql.SparkSession

class Context(var spark: SparkSession) {

  private val _sparkSession: SparkSession = spark
  private val _twitterTable: TwitterTable = new TwitterTable()
  private val _processPipeline: ProcessPipeline = new ProcessPipeline()
  private val _loadOffset: LoadOffset = new LoadOffset()

  def sparkSession: SparkSession = {
    _sparkSession
  }

  def twitterTable: TwitterTable = {
    _twitterTable
  }

  def processPipeline: ProcessPipeline = {
    _processPipeline
  }

  def runStepList(): Unit = {
    _processPipeline.runStepList(this)
  }

  def partitionOffset(): String = {
    _loadOffset.partitionOffset
  }

}
