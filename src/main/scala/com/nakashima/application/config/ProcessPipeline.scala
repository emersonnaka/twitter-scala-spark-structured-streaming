package com.nakashima.application.config

import com.nakashima.application.interfaces.Step
import com.nakashima.application.pipeline.dataquality.{CreatePartitionColumn, FormatDate, FormatFields}
import com.nakashima.application.pipeline.kafka.SourceReadStream
import com.nakashima.application.pipeline.save.SaveStorage
import com.nakashima.application.pipeline.schema.TwitterSchema

class ProcessPipeline {

  private val _stepList: List[Step] = buildStepList()

  private def buildStepList(): List[Step] = {
    List(
      new SourceReadStream,
      new TwitterSchema,
      new FormatDate,
      new CreatePartitionColumn,
      new FormatFields,
      new SaveStorage
    )
  }

  def runStepList(context: Context): Unit = {
    for (step <- _stepList) {
      step.process(context)
    }
  }

}
