package com.nakashima.application.enumeration

object PropertiesEnum extends Enumeration {

  case class PropertiesValue(value: String) extends super.Val

  val FileType = PropertiesValue("config.storage.file_type")
  val KafkaBootstrapServer = PropertiesValue("config.kafka.server")
  val KafkaTopic = PropertiesValue("config.kafka.topic")
  val SparkAppName = PropertiesValue("config.spark.app-name")
  val SparkCheckpointLocation = PropertiesValue("config.spark.checkpoint_location")
  val StoragePath = PropertiesValue("config.storage.path")
}
