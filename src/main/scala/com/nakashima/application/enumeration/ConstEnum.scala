package com.nakashima.application.enumeration

object ConstEnum extends Enumeration {

  case class ConstValue(value: String) extends super.Val

  val Backup = ConstValue("backup")
  val Day = ConstValue("day")
  val Earliest = ConstValue("earliest")
  val Month = ConstValue("month")
  val Sep = ConstValue(">")
  val Storage = ConstValue("storage")
  val TimestampFormat = ConstValue("MMM d, yyyy h:mm:ss a")
  val Year = ConstValue("year")

}
