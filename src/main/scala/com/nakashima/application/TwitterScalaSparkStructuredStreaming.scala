package com.nakashima.application

import com.nakashima.application.config.Context
import org.apache.spark.sql.SparkSession
import com.nakashima.application.enumeration.PropertiesEnum.{SparkAppName, SparkCheckpointLocation}
import com.nakashima.common.PropertiesReader.getString;

object TwitterScalaSparkStructuredStreaming {

  def createSparkSession(): SparkSession = {
    SparkSession
      .builder()
      .appName(getString(SparkAppName.value))
      .master("local")
      .config("spark.sql.streaming.checkpointLocation", getString(SparkCheckpointLocation.value))
      .getOrCreate();
  }

  def main(args: Array[String]): Unit = {
    val sparkSession = createSparkSession()
    sparkSession.sparkContext.setLogLevel("ERROR")
    new Context(sparkSession).runStepList()
    sparkSession.streams.awaitAnyTermination()
  }

}
