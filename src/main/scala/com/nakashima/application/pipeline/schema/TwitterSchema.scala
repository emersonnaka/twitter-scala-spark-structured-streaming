package com.nakashima.application.pipeline.schema

import com.nakashima.application.config.Context
import com.nakashima.application.interfaces.Step
import org.apache.spark.sql.functions.from_json

class TwitterSchema extends Step {

  override def process(context: Context): Unit = {
    val twitterTable = context.twitterTable
    val rawDs = twitterTable.ds

    var ds = rawDs.select(
      from_json(
        rawDs.col("value").cast("string"),
        twitterTable.schema
      ).as("twitter"))

    ds = ds.select("twitter.*")

    twitterTable.ds_(ds)
  }

}
