package com.nakashima.application.pipeline.kafka

import com.nakashima.application.config.Context
import com.nakashima.application.interfaces.Step
import com.nakashima.application.enumeration.PropertiesEnum.{KafkaBootstrapServer, KafkaTopic}
import com.nakashima.common.PropertiesReader.getString;

class SourceReadStream extends Step {

  override def process(context: Context): Unit = {
    val ds = context.sparkSession
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", getString(KafkaBootstrapServer.value))
      .option("subscribe", getString(KafkaTopic.value))
      .option("startingOffsets", context.partitionOffset())
      .option("failOnDataLoss", false)
      .load()

    context.twitterTable.ds_(ds)
  }

}
