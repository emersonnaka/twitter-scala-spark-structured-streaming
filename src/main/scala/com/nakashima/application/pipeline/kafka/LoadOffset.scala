package com.nakashima.application.pipeline.kafka

import com.nakashima.application.enumeration.ConstEnum.{Backup, Earliest, Storage}
import com.nakashima.application.enumeration.PropertiesEnum.SparkCheckpointLocation
import com.nakashima.common.PropertiesReader.getString
import com.nakashima.common.StorageSystem

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class LoadOffset {

  private val _regex = "^\\{\\\"[a-zA-Z_\\-]+\\\":\\{\\\"\\d+\\\":\\s*\\d+(,\\s*\\\"\\d+\\\":\\d+)*\\}\\}$"
  private val _checkpointPath = getString(SparkCheckpointLocation.value) + Storage.value
  private val _partitionOffset = readCheckpointOffset()

  private def readCheckpointOffset(): String = {
    var json = Earliest.value
    if (!StorageSystem.pathExists(_checkpointPath)) {
      return json
    }

    val offsetFilePath = lastOffsetFilePath()
    json = partitionOffsetJson(offsetFilePath)

    if (!json.equals(Earliest.value)) {
      moveCheckpointToBackup();
    }

    json
  }

  private def lastOffsetFilePath(): String = {
    val offsetPath = _checkpointPath + "/offsets/"
    StorageSystem.lastFileByModificationTime(offsetPath)
  }

  private def partitionOffsetJson(filePath: String): String = {
    var json = Earliest.value
    val bf = StorageSystem.contentFile(filePath)
    var line = bf.readLine()

    while(line != null) {
      if (line.matches(_regex)) {
        json = line
      }
      line = bf.readLine()
    }

    json
  }

  private def moveCheckpointToBackup(): Unit = {
    val backupStrPath = backupPath()
    if (StorageSystem.mkdirs(backupStrPath)) {
      StorageSystem.moveFiles(_checkpointPath, backupStrPath)
    }
  }

  private def backupPath(): String = {
    val timeFormat = "yyyy_MM_dd_HH_mm_ss"
    val execTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat))
    getString(SparkCheckpointLocation.value) + Backup.value + "/" + execTime + "/"
  }

  def partitionOffset: String = {
    _partitionOffset
  }

}
