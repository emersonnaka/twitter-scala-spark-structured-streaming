package com.nakashima.application.pipeline.dataquality

import com.nakashima.application.config.Context
import com.nakashima.application.enumeration.ConstEnum.{Day, Month, Year}
import com.nakashima.application.enumeration.TwitterMessageEnum.CreatedAt
import com.nakashima.application.interfaces.Step
import org.apache.spark.sql.functions.{dayofmonth, month, year}

class CreatePartitionColumn extends Step {

  override def process(context: Context): Unit = {
    val twitterTable = context.twitterTable
    var ds = twitterTable.ds

    ds = ds.withColumn(Year.value, year(ds.col(CreatedAt.schemaField)))
      .withColumn(Month.value, month(ds.col(CreatedAt.schemaField)))
      .withColumn(Day.value, dayofmonth(ds.col(CreatedAt.schemaField)))

    twitterTable.ds_(ds)
  }

}
