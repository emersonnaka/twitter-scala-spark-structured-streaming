package com.nakashima.application.pipeline.dataquality

import com.nakashima.application.config.Context
import com.nakashima.application.enumeration.ConstEnum.{Sep, TimestampFormat}
import com.nakashima.application.enumeration.TwitterMessageEnum.{CreatedAt, User}
import com.nakashima.application.interfaces.Step
import org.apache.spark.sql.functions.to_timestamp
import org.apache.spark.sql.{Column, Dataset, Row}

class FormatDate extends Step {

  override def process(context: Context): Unit = {
    val twitterTable = context.twitterTable
    var ds = twitterTable.ds
    val userCreatedAt = User.schemaField + Sep.value + CreatedAt.schemaField

    ds = formatDate(ds, CreatedAt.schemaField, userCreatedAt)

    twitterTable.ds_(ds)
  }

  private def formatDate(ds: Dataset[Row], fields: String*): Dataset[Row] = {
    var formatDs = ds
    for (field <- fields) {
      if (field.contains(Sep.value)) {
        val fieldArray = field.split(Sep.value)
        val correctField = fieldArray(0) + "." + fieldArray(1)
        formatDs = formatDs.withColumn(
          fieldArray(0),
          formatDs.col(fieldArray(0)).withField(
            fieldArray(1),
            castColumnToTimestamp(formatDs, correctField)))
      } else {
        formatDs = formatDs.withColumn(field, castColumnToTimestamp(ds, field))
      }
    }
    formatDs
  }

  private def castColumnToTimestamp(ds: Dataset[Row], field: String): Column = {
    to_timestamp(ds.col(field), TimestampFormat.value)
  }

}
