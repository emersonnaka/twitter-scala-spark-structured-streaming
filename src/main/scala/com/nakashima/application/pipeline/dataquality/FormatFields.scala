package com.nakashima.application.pipeline.dataquality

import com.nakashima.application.config.Context
import com.nakashima.application.enumeration.ConstEnum.{Day, Month, Year}
import com.nakashima.application.enumeration.TwitterMessageEnum._
import com.nakashima.application.interfaces.Step
import org.apache.spark.sql.types.{ArrayType, BooleanType, StructType}
import org.apache.spark.sql.types.DataTypes.{IntegerType, LongType, StringType, createArrayType}
import org.apache.spark.sql.{Dataset, Row}

class FormatFields extends Step {

  override def process(context: Context): Unit = {
    val twitterTable = context.twitterTable
    var ds = twitterTable.ds

    ds = renameArrayFields(ds)
    ds = formatFields(ds)

    twitterTable.ds_(ds)
  }

  private def renameArrayFields(ds: Dataset[Row]): Dataset[Row] = {
    ds.withColumnRenamed(UserMentionEntities.schemaField, UserMentionEntities.snakeCaseField)
      .withColumnRenamed(UrlEntities.schemaField, UrlEntities.snakeCaseField)
      .withColumnRenamed(HashtagEntities.schemaField, HashtagEntities.snakeCaseField)
      .withColumnRenamed(MediaEntities.schemaField, MediaEntities.snakeCaseField)
      .withColumnRenamed(SymbolEntities.schemaField, SymbolEntities.snakeCaseField)
  }

  private def formatFields(ds: Dataset[Row]): Dataset[Row] = {
    ds.select(
      ds.col(CreatedAt.schemaField).as(CreatedAt.snakeCaseField),
      ds.col(Id.schemaField),
      ds.col(Text.schemaField),
      ds.col(Source.schemaField),
      ds.col(IsTruncated.schemaField).as(IsTruncated.snakeCaseField),
      ds.col(InReplyToStatusId.schemaField).as(InReplyToStatusId.snakeCaseField),
      ds.col(InReplyToUserId.schemaField).as(InReplyToUserId.snakeCaseField),
      ds.col(IsFavorited.schemaField).as(IsFavorited.snakeCaseField),
      ds.col(IsRetweeted.schemaField).as(IsRetweeted.snakeCaseField),
      ds.col(FavoriteCount.schemaField).as(FavoriteCount.snakeCaseField),
      ds.col(InReplayToScreenName.schemaField).as(InReplayToScreenName.snakeCaseField),
      ds.col(RetweetCount.schemaField).as(RetweetCount.snakeCaseField),
      ds.col(IsPossiblySensitive.schemaField).as(IsPossiblySensitive.snakeCaseField),
      ds.col(Lang.schemaField),
      ds.col(UserMentionEntities.snakeCaseField).cast(renameUserMentionEntities()),
      ds.col(UrlEntities.snakeCaseField).cast(renameUrlEntities()),
      ds.col(HashtagEntities.snakeCaseField),
      ds.col(MediaEntities.snakeCaseField).cast(renameMediaEntities()),
      ds.col(SymbolEntities.snakeCaseField),
      ds.col(User.schemaField).cast(renameUser()),
      ds.col(QuotedStatusId.schemaField).as(QuotedStatusId.snakeCaseField),
      ds.col(Year.value),
      ds.col(Month.value),
      ds.col(Day.value)
    )
  }

  private def renameUserMentionEntities(): ArrayType = {
    createArrayType(
      new StructType()
        .add(Name.schemaField, StringType, true)
        .add(ScreenName.snakeCaseField, StringType, true)
        .add(Id.schemaField, LongType, true)
        .add(Start.schemaField, IntegerType, true)
        .add(End.schemaField, IntegerType, true)
    )
  }

  private def renameUrlEntities(): ArrayType = {
    createArrayType(
      new StructType()
        .add(Url.schemaField, StringType, true)
        .add(ExpandedUrl.snakeCaseField, StringType, true)
        .add(DisplayUrl.snakeCaseField, StringType, true)
        .add(Start.schemaField, IntegerType, true)
        .add(End.schemaField, IntegerType, true)
    )
  }

  private def renameMediaEntities(): ArrayType = {
    createArrayType(
      new StructType()
        .add(Id.schemaField, LongType, true)
        .add(Url.schemaField, StringType, true)
        .add(MediaUrl.snakeCaseField, StringType, true)
        .add(MediaUrlHttps.snakeCaseField, StringType, true)
        .add(ExpandedUrl.snakeCaseField, StringType, true)
        .add(DisplayUrl.snakeCaseField, StringType, true)
        .add(Sizes.schemaField, sizesSchema(), true)
        .add(Type.schemaField, StringType, true)
        .add(VideoAspectRatioWidth.snakeCaseField, IntegerType, true)
        .add(VideoAspectRatioHeight.snakeCaseField, IntegerType, true)
        .add(VideoDurationMillis.snakeCaseField, IntegerType, true)
        .add(VideoVariants.snakeCaseField, renameVideoVariants(), true)
        .add(Start.schemaField, IntegerType, true)
        .add(End.schemaField, IntegerType, true)
    )
  }

  private def sizesSchema(): StructType = {
    new StructType()
      .add(Zero.schemaField, sizeSchema(), true)
      .add(One.schemaField, sizeSchema(), true)
      .add(Two.schemaField, sizeSchema(), true)
      .add(Three.schemaField, sizeSchema(), true)
  }

  private def sizeSchema(): StructType = {
    new StructType()
      .add(Width.schemaField, IntegerType, true)
      .add(Height.schemaField, IntegerType, true)
      .add(Resize.schemaField, IntegerType, true)
  }

  private def renameVideoVariants(): ArrayType = {
    createArrayType(
      new StructType()
        .add(Bitrate.schemaField, IntegerType, true)
        .add(ContentType.snakeCaseField, StringType, true)
        .add(Url.schemaField, StringType, true)
    )
  }

  private def renameUser(): StructType = {
    new StructType()
      .add(Id.schemaField, LongType, true)
      .add(Name.schemaField, StringType, true)
      .add(ScreenName.snakeCaseField, StringType, true)
      .add(Description.schemaField, StringType, true)
      .add(DescriptionUrlEntities.snakeCaseField, renameDescriptionUrlEntities(), true)
      .add(IsContributorsEnabled.snakeCaseField, BooleanType, true)
      .add(ProfileImageUrl.snakeCaseField, StringType, true)
      .add(ProfileImageUrlHttps.snakeCaseField, StringType, true)
      .add(IsDefaultProfileImage.snakeCaseField, BooleanType, true)
      .add(IsProtected.snakeCaseField, BooleanType, true)
      .add(FollowersCount.snakeCaseField, IntegerType, true)
      .add(ProfileBackgroundColor.snakeCaseField, StringType, true)
      .add(ProfileTextColor.snakeCaseField, StringType, true)
      .add(ProfileLinkColor.snakeCaseField, StringType, true)
      .add(ProfileSidebarFillColor.snakeCaseField, StringType, true)
      .add(ProfileSidebarBorderColor.snakeCaseField, StringType, true)
      .add(ProfileUseBackgroundImage.snakeCaseField, BooleanType, true)
      .add(IsDefaultProfile.snakeCaseField, BooleanType, true)
      .add(ShowAllInlineMedia.snakeCaseField, BooleanType, true)
      .add(FriendsCount.snakeCaseField, IntegerType, true)
      .add(CreatedAt.snakeCaseField, StringType, true)
      .add(FavouritesCount.snakeCaseField, IntegerType, true)
      .add(UtcOffset.snakeCaseField, IntegerType, true)
      .add(ProfileBackgroundImageUrl.snakeCaseField, StringType, true)
      .add(ProfileBackgroundImageUrlHttps.snakeCaseField, StringType, true)
      .add(ProfileBannerImageUrl.snakeCaseField, StringType, true)
      .add(ProfileBackgroundTiled.snakeCaseField, BooleanType, true)
      .add(StatusesCount.snakeCaseField, IntegerType, true)
      .add(IsGeoEnabled.snakeCaseField, BooleanType, true)
      .add(IsVerified.snakeCaseField, BooleanType, true)
      .add(Translator.schemaField, BooleanType, true)
      .add(ListedCount.snakeCaseField, IntegerType, true)
      .add(IsFollowRequestSent.snakeCaseField, BooleanType, true)
  }

  private def renameDescriptionUrlEntities(): ArrayType = {
    createArrayType(
      new StructType()
        .add(Url.schemaField, StringType, true)
        .add(ExpandedUrl.snakeCaseField, StringType, true)
        .add(DisplayUrl.snakeCaseField, StringType, true)
    )
  }

}
