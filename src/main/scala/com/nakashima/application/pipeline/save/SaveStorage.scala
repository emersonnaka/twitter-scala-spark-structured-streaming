package com.nakashima.application.pipeline.save

import com.nakashima.application.config.Context
import com.nakashima.application.enumeration.ConstEnum.{Day, Month, Storage, Year}
import com.nakashima.application.enumeration.PropertiesEnum.{FileType, StoragePath}
import com.nakashima.application.enumeration.TwitterMessageEnum.Lang
import com.nakashima.application.interfaces.Step
import com.nakashima.common.PropertiesReader.getString
import org.apache.spark.sql.streaming.{OutputMode, Trigger}

class SaveStorage extends Step {

  override def process(context: Context): Unit = {
    val twitterTable = context.twitterTable
    val ds = twitterTable.ds

    ds.writeStream
      .queryName(Storage.value)
      .outputMode(OutputMode.Append())
      .format(getString(FileType.value))
      .option("path", getString(StoragePath.value))
      .partitionBy(
        Lang.schemaField,
        Year.value,
        Month.value,
        Day.value
      )
      .trigger(Trigger.ProcessingTime("30 seconds"))
      .start()
  }

}
