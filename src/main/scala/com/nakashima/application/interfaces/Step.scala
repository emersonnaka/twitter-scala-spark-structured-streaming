package com.nakashima.application.interfaces

import com.nakashima.application.config.Context

trait Step {
  def process(context: Context): Unit
}
