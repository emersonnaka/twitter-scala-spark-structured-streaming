package com.nakashima.common

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

import java.io.{BufferedReader, InputStreamReader}

object StorageSystem {

  def fileSystem: FileSystem = {
    FileSystem.get(new Configuration())
  }

  def pathExists(strPath: String): Boolean = {
    val fs = fileSystem
    val path = new Path(strPath)
    fs.exists(path)
  }

  def lastFileByModificationTime(strPath: String): String = {
    var filePath = ""
    var lastFileCreated = Long.MinValue
    val fs = fileSystem
    val path = new Path(strPath)

    for (fileStatus <- fs.listStatus(path)) {
      if (fileStatus.getModificationTime > lastFileCreated) {
        filePath = fileStatus.getPath.toString
        lastFileCreated = fileStatus.getModificationTime
      }
    }

    filePath
  }

  def contentFile(filePath: String): BufferedReader = {
    val fs = fileSystem
    val path = new Path(filePath)
    new BufferedReader(new InputStreamReader(fs.open(path)))
  }

  def mkdirs(strPath: String): Boolean = {
    val fs = fileSystem
    val path = new Path(strPath)
    fs.mkdirs(path)
  }

  def moveFiles(source: String, to: String): Unit = {
    val fs = fileSystem
    val sourcePath = new Path(source)
    val toPath = new Path(to)

    for (fileStatus <- fs.listStatus(sourcePath)) {
      fs.moveFromLocalFile(fileStatus.getPath, toPath)
    }
  }

}
