package com.nakashima.common

import com.typesafe.config.ConfigFactory

object PropertiesReader {

  val Conf = ConfigFactory.load();

  def getString(key: String): String = {
    return Conf.getString(key);
  }

}
